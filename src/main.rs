use std::time::Duration;
use tokio::io::{self, AsyncWriteExt};
use tokio::process::Command;
use tokio::task;
use tokio::time;

#[tokio::main]
async fn main() -> Result<(), std::io::Error> {
    let mut stdout = io::stdout();
    let mut hello: &[u8] = b"Hello, world!\n";
    io::copy(&mut hello, &mut stdout).await?;
    stdout.write_all(b"Hello, world again!\n").await?;

    // exercise 1: copy stdin to stdout
//    let mut stdin = io::stdin();
//    io::copy(&mut stdin, &mut stdout).await?;

    // spawning processes
    Command::new("echo").arg("Hello, world from command").spawn()?.await?;

    // exercise 2: print string 10 times
    // for _ in 1..10 {
    //     Command::new("echo").arg("Hello, world from command").spawn()?.await?;
    // }


    // take a break
    Command::new("date").spawn()?.await?;
    time::delay_for(Duration::from_secs(1)).await;
    Command::new("date").spawn()?.await?;
    time::delay_for(Duration::from_secs(1)).await;
    Command::new("date").spawn()?.await?;
    time::delay_for(Duration::from_secs(1)).await;

    // let mut interval = time::interval(Duration::from_secs(1));
    // loop {
    //     interval.tick().await;
    //     Command::new("date").spawn()?.await?;
    // }

    task::spawn(dating()).await??;
    Ok(())
}

async fn dating() -> Result<(), std::io::Error> {
    let mut interval = time::interval(Duration::from_secs(1));
    loop {
        interval.tick().await;
        Command::new("date").spawn()?.await?;
    }
}
